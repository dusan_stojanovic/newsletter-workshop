-- Initial public schema relates to eCommerce 0.x

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA pg_catalog;

SET search_path = public, pg_catalog;
SET default_tablespace = '';

-- update updated at column
CREATE OR REPLACE FUNCTION update_updated_at_column() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    NEW.updated_at = now();
    RETURN NEW;
END;
$$;

-- enum_user_type
CREATE TABLE enum_user_type (
    code integer NOT NULL,
    user_type text NOT NULL,
    CONSTRAINT enum_user_type_pk PRIMARY KEY (code)
);

-- user
CREATE TABLE system_user (
    user_uid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
    username text not null,
    first_name text not null,
    last_name text not null,
    user_type integer NOT NULL,
    is_admin boolean DEFAULT false,
    CONSTRAINT user_pk PRIMARY KEY (user_uid),
    CONSTRAINT fk_user_user_type FOREIGN KEY (user_type)
        REFERENCES enum_user_type (code) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

-- post
CREATE TABLE post (
    post_uid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
    author_uid uuid NOT NULL,
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    updated_at timestamp with time zone NOT NULL DEFAULT now(),
    title text NOT NULL,
    content text NOT NULL,
    CONSTRAINT post_pk PRIMARY KEY (post_uid),
    CONSTRAINT fk_post_author_uid FOREIGN KEY (author_uid)
        REFERENCES system_user (user_uid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TRIGGER update_post_updated_at_column
    BEFORE UPDATE
    ON post
    FOR EACH ROW
    EXECUTE PROCEDURE update_updated_at_column();

-- fill enum_user_type table
INSERT INTO enum_user_type VALUES
    (0, 'subscriber'),
    (1, 'author');

-- fill user table with admin user
INSERT INTO system_user (username, first_name, last_name, user_type, is_admin)
VALUES ('admin', 'admin', 'admin', 1, true);
