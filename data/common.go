package data

import (
	"Newsletter-Workshop/server/dbserver"
	"Newsletter-Workshop/values"
	"context"
	"time"
)

func executeQueryWithBoolResponse(ctx context.Context, query string, params ...interface{}) (result bool, err error) {
	dbRunner := ctx.Value(values.ContextKeyDbRunner).(dbserver.Runner)

	rows, err := dbRunner.Query(ctx, query, params...)
	if err != nil {
		return
	}

	defer rows.Close()

	rr, err := dbserver.GetRowReader(rows)
	if err != nil {
		return
	}

	if rr.ScanNext() {
		result = rr.ReadByIdxBool(0)
	}

	err = rr.Error()

	return
}

func executeQueryWithStringResponse(ctx context.Context, query string, params ...interface{}) (result string, err error) {
	dbRunner := ctx.Value(values.ContextKeyDbRunner).(dbserver.Runner)

	rows, err := dbRunner.Query(ctx, query, params...)
	if err != nil {
		return
	}

	defer rows.Close()

	rr, err := dbserver.GetRowReader(rows)
	if err != nil {
		return
	}

	if rr.ScanNext() {
		result = rr.ReadByIdxString(0)
	}

	err = rr.Error()

	return
}

func executeQueryWithTimeResponse(ctx context.Context, query string, params ...interface{}) (result time.Time, err error) {
	dbRunner := ctx.Value(values.ContextKeyDbRunner).(dbserver.Runner)

	rows, err := dbRunner.Query(ctx, query, params...)
	if err != nil {
		return
	}

	defer rows.Close()

	rr, err := dbserver.GetRowReader(rows)
	if err != nil {
		return
	}

	if rr.ScanNext() {
		result = rr.ReadByIdxTime(0)
	}

	err = rr.Error()

	return
}
