package data

import (
	"Newsletter-Workshop/server/dbserver"
	"Newsletter-Workshop/values"
	"context"
)

var (
	//CreateUser creates new user
	CreateUser = createUser

	//GetUser returns user
	GetUser = getUser

	//GetAllUsers retirns list of users
	GetAllUsers = getAllUsers

	//EditUser updates user
	EditUser = editUser

	//DeleteUser deletes user
	DeleteUser = deleteUser

	// UsernameExists return true if username exists
	UsernameExists = usernameExists

	// AuthUser returns user auth token (userUID)
	AuthUser = authUser
)

func createUser(ctx context.Context, username, firstName, lastName string, userType int, isAdmin bool) (response *UserEntity, err error) {
	dbRunner := ctx.Value(values.ContextKeyDbRunner).(dbserver.Runner)

	query := `
		insert into system_user(username, first_name, last_name, user_type, is_admin)
		values ($1, $2, $3, $4, $5)
		returning user_uid`

	rows, err := dbRunner.Query(ctx, query, username, firstName, lastName, userType, isAdmin)
	if err != nil {
		return
	}

	defer rows.Close()

	rr, err := dbserver.GetRowReader(rows)
	if err != nil {
		return
	}

	if rr.ScanNext() {
		response = &UserEntity{}
		response.UserUID = rr.ReadByIdxString(0)
		response.Username = username
		response.FirstName = firstName
		response.LastName = lastName
		response.UserType = userType
		response.IsAdmin = isAdmin
	}

	err = rr.Error()

	return
}

func getUser(ctx context.Context, userUID string) (response *UserEntity, err error) {
	dbRunner := ctx.Value(values.ContextKeyDbRunner).(dbserver.Runner)

	query := `
		select
			user_uid as "UserUID",
			username as "Username",	
			first_name as "FirstName",
			last_name as "LastName",
			user_type as "UserType",
			is_admin as "IsAdmin"
		from system_user
		where user_uid = $1`

	rows, err := dbRunner.Query(ctx, query, userUID)
	if err != nil {
		return
	}

	defer rows.Close()

	rr, err := dbserver.GetRowReader(rows)
	if err != nil {
		return
	}

	if rr.ScanNext() {
		response = &UserEntity{}
		rr.ReadAllToStruct(response)
	}

	err = rr.Error()

	return
}

func getAllUsers(ctx context.Context, searchTerm string, rowOffset, rowLimit int) (users []*UserEntity, err error) {
	dbRunner := ctx.Value(values.ContextKeyDbRunner).(dbserver.Runner)

	query := `
		select
			user_uid as "UserUID",
			username as "Username",	
			first_name as "FirstName",
			last_name as "LastName",
			user_type as "UserType",
			is_admin as "IsAdmin"
		from system_user
		where username like '%%' || $1 || '%%'
		offset $2
		limit $3`

	rows, err := dbRunner.Query(ctx, query, searchTerm, rowOffset, rowLimit)
	if err != nil {
		return
	}

	defer rows.Close()

	rr, err := dbserver.GetRowReader(rows)
	if err != nil {
		return
	}

	users = make([]*UserEntity, 0)
	for rr.ScanNext() {
		user := &UserEntity{}
		rr.ReadAllToStruct(user)
		users = append(users, user)
	}

	err = rr.Error()

	return
}

func editUser(ctx context.Context, userUID, username, firstName, lastName string, userType int, isAdmin bool) (err error) {
	dbRunner := ctx.Value(values.ContextKeyDbRunner).(dbserver.Runner)

	query := `
		update system_user
		set
			username = $1,
			first_name = $2,
			last_name = $3,
			user_type = $4,
			is_admin = $5
		where user_uid = $6`

	_, err = dbRunner.Exec(ctx, query, username, firstName, lastName, userType, isAdmin, userUID)

	return
}

func deleteUser(ctx context.Context, userUID string) (err error) {
	dbRunner := ctx.Value(values.ContextKeyDbRunner).(dbserver.Runner)

	query := `delete from system_user where user_uid = $1`

	_, err = dbRunner.Exec(ctx, query, userUID)

	return
}

func usernameExists(ctx context.Context, username string) (response bool, err error) {
	query := `select exists (select 1 from system_user where username = $1)`

	return executeQueryWithBoolResponse(ctx, query, username)
}

func authUser(ctx context.Context, username string) (response string, err error) {
	query := `select user_uid from system_user where username = $1`

	return executeQueryWithStringResponse(ctx, query, username)
}
