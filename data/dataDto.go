package data

// UserEntity is struct used to describe system user
type UserEntity struct {
	UserUID   string
	Username  string
	FirstName string
	LastName  string
	UserType  int
	IsAdmin   bool
}
