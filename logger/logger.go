package logger

import (
	"log"
	"time"
)

var (
	// LogRequest will log http request
	LogRequest = logRequest

	// LogResponse will log http response
	LogResponse = logResponse

	// LogError will log error
	LogError = logError
)

func logRequest(startTime time.Time, status int, method, uri, requestBody string, duration time.Duration) {
	log.Printf("%v: status=%v method=%v uri=%v duration=%v", startTime, status, method, uri, duration)
	log.Printf("request=%v", requestBody)
}

func logResponse(response string) {
	log.Printf("response=%v", response)
}

func logError(cause string, err error) {
	if err != nil {
		log.Printf("error: %v: %v", cause, err)
	} else {
		log.Printf("error: %v", cause)
	}
}
