package server

import (
	"Newsletter-Workshop/config"
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

var (
	// StartHTTPServer starts listening to HTTP request
	StartHTTPServer = startHTTPServer
)

func startHTTPServer() (err error) {
	mux := http.NewServeMux()
	mux.Handle("/api/", newHandlerAPI())

	server := http.Server{}

	server.ReadTimeout = config.GetHTTPReadTimeout()
	server.WriteTimeout = config.GetHTTPWriteTimeout()
	server.Handler = mux

	go func() {
		// Listen to operating system's interrupt signal
		interrupt := make(chan os.Signal, 1)
		signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)
		<-interrupt

		// Gracefully shut down the server when it happens
		if err := server.Shutdown(context.Background()); err != nil {
			log.Printf("Error shutting down. %v\n", err)
		}
	}()

	server.Addr = config.GetHTTPServerAddress()
	err = server.ListenAndServe()

	if err == http.ErrServerClosed {
		err = nil
	}

	return
}
