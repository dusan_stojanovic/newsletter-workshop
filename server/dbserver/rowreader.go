package dbserver

import (
	"Newsletter-Workshop/null"
	"database/sql"
	"errors"
	"reflect"
	"strconv"
	"time"
)

var (
	// GetRowReader returns RowReader interface that can be used to read data from sql.Rows
	GetRowReader = getRowReader
)

type rowReader struct {
	rows      *sql.Rows
	columns   []string
	values    []interface{}
	valuePtrs []interface{}
	colIdxMap map[string]int
	lastError error
}

// RowReader is used to simplify reading sql.Rows object
type RowReader interface {
	ScanNext() bool
	Error() error
	RowReadFxs
}

// RowReadFxs contains methods to read values from a single row in sql.Rows
type RowReadFxs interface {
	ReadByIdxString(columnIdx int) string
	ReadByIdxNullString(columnIdx int) null.String
	ReadByIdxBool(columnIdx int) bool
	ReadByIdxInt64(columnIdx int) int64
	ReadByIdxTime(columnIdx int) time.Time
	ReadAllToStruct(p interface{})
}

// Errors
var (
	ErrorNullValue   = errors.New("Null value encountered")
	ErrorWrongType   = errors.New("Unable to convert type")
	ErrorUnsupported = errors.New("Unsupported type")
)

// Types
var (
	typeNullableString = reflect.TypeOf(null.String{})
)

func getRowReader(rows *sql.Rows) (RowReader, error) {
	columns, err := rows.Columns()
	if err != nil {
		return nil, err
	}

	n := len(columns)

	rr := new(rowReader)
	rr.rows = rows
	rr.columns = columns
	rr.values = make([]interface{}, n)
	rr.valuePtrs = make([]interface{}, n)

	for i := 0; i < n; i++ {
		rr.valuePtrs[i] = &rr.values[i]
	}

	return rr, nil
}

func (rr *rowReader) Error() error {
	return rr.lastError
}

func (rr *rowReader) ScanNext() (hasMore bool) {
	if hasMore = rr.rows.Next(); hasMore {
		err := rr.rows.Scan(rr.valuePtrs...)
		rr.lastError = err
		if err != nil {
			hasMore = false
		}
	}

	return
}

// String readers
func (rr *rowReader) ReadByIdxString(columnIdx int) string {
	switch value := rr.values[columnIdx].(type) {
	case []byte:
		return string(value)
	case string:
		return value
	case nil:
		panic(ErrorNullValue)
	default:
		panic(ErrorWrongType)
	}
}

func (rr *rowReader) ReadByIdxNullString(columnIdx int) null.String {
	var x null.String
	err := x.Scan(rr.values[columnIdx])
	if err != nil {
		panic(err)
	}

	return x
}

func (rr *rowReader) ReadByIdxBool(columnIdx int) bool {
	switch v := rr.values[columnIdx].(type) {
	case bool:
		return v
	case []byte:
		s := string(v)
		b, err := strconv.ParseBool(s)
		if err != nil {
			panic(ErrorWrongType)
		}
		return b
	case int64:
		return v != 0
	case float64:
		return v != 0.0
	case nil:
		panic(ErrorNullValue)
	default:
		panic(ErrorWrongType)
	}
}

func (rr *rowReader) ReadByIdxInt64(columnIdx int) int64 {
	switch v := rr.values[columnIdx].(type) {
	case int64:
		return v
	case []byte:
		s := string(v)
		i, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(ErrorWrongType)
		}
		return i
	case nil:
		panic(ErrorNullValue)
	default:
		panic(ErrorWrongType)
	}
}

func (rr *rowReader) ReadByIdxTime(columnIdx int) time.Time {
	switch v := rr.values[columnIdx].(type) {
	case time.Time:
		return v
	case []byte:
		q, err := time.Parse(time.RFC3339Nano, string(v))
		if err != nil {
			panic(ErrorWrongType)
		}
		return q
	case string:
		q, err := time.Parse(time.RFC3339Nano, v)
		if err != nil {
			panic(ErrorWrongType)
		}
		return q
	case nil:
		panic(ErrorNullValue)
	default:
		panic(ErrorWrongType)
	}
}

func (rr *rowReader) ReadAllToStruct(p interface{}) {
	var value reflect.Value

	value = reflect.ValueOf(p)
	if value.Kind() != reflect.Ptr {
		return
	}

	value = reflect.Indirect(value)
	if value.Kind() != reflect.Struct {
		return
	}

	for columnIdx, columnName := range rr.columns {
		if rr.values[columnIdx] == nil {
			continue
		}

		column := value.FieldByName(columnName)
		if column == (reflect.Value{}) {
			continue
		}

		columnKind := column.Kind()
		switch columnKind {
		case reflect.String:
			column.SetString(rr.ReadByIdxString(columnIdx))
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			column.SetInt(rr.ReadByIdxInt64(columnIdx))
		case reflect.Bool:
			column.SetBool(rr.ReadByIdxBool(columnIdx))
		case reflect.Struct:
			columnType := column.Type()
			switch columnType {
			case typeNullableString:
				column.Set(reflect.ValueOf(rr.ReadByIdxNullString(columnIdx)))
			case reflect.TypeOf(time.Time{}):
				column.Set(reflect.ValueOf(rr.ReadByIdxTime(columnIdx)))
			}
		default:
			panic(ErrorUnsupported)
		}
	}
}
