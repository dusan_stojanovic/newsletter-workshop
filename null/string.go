package null

import "database/sql"

var (
	// NewNullableString returns a null String if the parameter is empty, a valid String otherwise
	NewNullableString = newNullableString

	// GetNullStringValue return empty sting if null.String is null, a valid String othervise
	GetNullStringValue = getNullStringValue
)

// String represents nullable string. Intended for use in fetching data from SQL queries
type String struct {
	sql.NullString
}

func newNullableString(x string) String {
	if x == "" {
		return String{}
	}

	return String{sql.NullString{String: x, Valid: true}}
}

func getNullStringValue(x String) string {
	if x.Valid == false {
		return ""
	}

	return x.String
}
