package values

// ContextKeyDbRunner is a key for context.Context to extract db runner.
var ContextKeyDbRunner = contextKeyDbRunner{}

// ContextKeyUserUID is a key for context.Context to extract userUID.
var ContextKeyUserUID = contextKeyUserUID{}

type contextKeyDbRunner struct{}

type contextKeyUserUID struct{}
