package values

// Values for user type
const (
	UserTypeSubscriber = 0
	UserTypeAuthor     = 1
)

// MaxRowLimit is max row limit for get all API
const MaxRowLimit = 1000
