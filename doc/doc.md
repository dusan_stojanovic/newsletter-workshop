### User API

## Create User

Creates new user.

URI: /api/user/create

Request:
* Username (string)
* FirstName (string)
* LastName (string)
* UserType (int): 0 - subscriber; 1 - author
* IsAdmin (bool): only admin user can create new admin user

Response:
* UserUID (string)
* Username (string)
* FirstName (string)
* LastName (string)
* UserType (int)
* IsAdmin (bool)

## Get User

Get information about user.
Admin user can get information about any users.
Other users can only see their information.

URI: /api/user/get

Request:
* UserUID (string)

Response:
* UserUID (string)
* Username (string)
* FirstName (string)
* LastName (string)
* UserType (int)
* IsAdmin (bool)

## Get All User

Get information about all users.
Only admin user can call this api.

URI: /api/user/getAll

Request:
* SearchTerm (string?): users will be filtered by username
* RowOffset (int?)
* RowLimit (int?)

Response:
* Meta: request
* Data

Data is array of:
* UserUID (string)
* Username (string)
* FirstName (string)
* LastName (string)
* UserType (int)
* IsAdmin (bool)

## Edit User

Edit information about user.
Admin user can edit information about any users.
Other users can only edit their information.

URI: /api/user/edit

Request:
* UserUID (string)
* Username (string)
* FirstName (string)
* LastName (string)
* UserType (int)
* IsAdmin (bool)

## Delete User

Delete information about user.
Only admin user can call this api.

URI: /api/user/delete

Request:
* UserUID (string)

## Auth User

Returns user's authentication token.
All users can call this api.

URI: /api/user/auth

Request:
* Username (string)

### Post API

## Create Post

Creates new post.
Only author users can call this api.

URI: /api/post/create

Request:
* AuthorUID (string)
* Title (string)
* Content (string)

Response:
* PostUID (string)
* CreatedAt (timestamp)
* UpdatedAt (timestamo)
* Title (string)
* AuthorUID (string)
* Content (string)

## Get Post

Get information about post.

URI: /api/post/get

Request:
* PostUID (string)

Response:
* PostUID (string)
* CreatedAt (timestamp)
* UpdatedAt (timestamo)
* Title (string)
* AuthorName (string)
* Content (string)

## Get All Posts

Get information about all posts.

URI: /api/post/getAll

Request:
* SearchTerm (string?): users will be filtered by title
* RowOffset (int?)
* RowLimit (int?)

Response:
* Meta: request
* Data

Data is array of:
* PostUID (string)
* CreatedAt (timestamp)
* UpdatedAt (timestamo)
* Title (string)
* AuthorName (string)

## Edit Post

Edit information about post.
Only author users can call this api.

URI: /api/post/edit

Request:
* PostUID (string)
* Title (string)
* Content (string)

Response:
* UpdatedAt (timestamp)

## Delete Post

Delete information about post.
Only author user can call this api.

URI: /api/post/delete

Request:
* PostUID (string)
