package main

import (
	"Newsletter-Workshop/config"
	"Newsletter-Workshop/server"
	"Newsletter-Workshop/server/dbserver"
	"log"
	"sync"

	_ "github.com/lib/pq"
)

func main() {
	log.Println("Starting Newsletter Server")

	log.Println("Initializing configuration")
	err := config.InitConfig("newsletter", nil)
	if err != nil {
		log.Fatalf("Failed to read configuration: %v\n", err)
	}

	log.Println("Initializing database")
	err = dbserver.InitializeDb()
	if err != nil {
		log.Fatalf("Could not access database: %v\n", err)
	}

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer wg.Done()

		log.Println("Starting HTTP server")
		err := server.StartHTTPServer()
		if err != nil {
			log.Fatalf("Could not start HTTP server: %v\n", err)
			return
		}

		log.Println("HTTP server gracefully terminated")
	}()

	wg.Wait()

	log.Println("Newsletter server stopped")
}
