package uservalidation

import (
	"Newsletter-Workshop/server/dbserver"
	"Newsletter-Workshop/values"
	"context"
)

var (
	// ValidateUser checks user and return user type and admin permissions if user is valid
	ValidateUser = validateUser
)

// UserInfo is struct used to return all user information required for validation
type UserInfo struct {
	UserType int
	IsAdmin  bool
}

func validateUser(ctx context.Context, userUID string) (response *UserInfo, err error) {
	dbRunner := ctx.Value(values.ContextKeyDbRunner).(dbserver.Runner)

	query := `
			select
				user_type as "UserType",
				is_admin as "IsAdmin"
			from system_user
			where user_uid = $1`

	rows, err := dbRunner.Query(ctx, query, userUID)
	if err != nil {
		return
	}

	defer rows.Close()

	rr, err := dbserver.GetRowReader(rows)
	if err != nil {
		return
	}

	if rr.ScanNext() {
		response = &UserInfo{}
		rr.ReadAllToStruct(response)
	}

	err = rr.Error()

	return
}
