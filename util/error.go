package util

import (
	"Newsletter-Workshop/logger"
	"context"
	"errors"
	"net/http"
)

var (
	// MapErrorTypeToHTTPStatus maps errors to corresponding HTTP status code
	MapErrorTypeToHTTPStatus = mapErrorTypeToHTTPStatus

	// IsError returns underlying error type
	IsError = isError

	// NewError creates new Error object
	NewError = newError
)

var (
	// ErrBadRequest is returned when when there is something wrong with the request itself (i.e. invalid request body)
	// Should be returned as 400 HTTP Status
	ErrBadRequest = errors.New("Bad request")

	// ErrInternal is returned when something bad happened during processing API request
	// Should be returned as 500 HTTP Status
	ErrInternal = errors.New("Internal error")

	// ErrInvalidAPICall is returned when API call issued to server is invalid (API is not supported or URI is invalid)
	// Should be returned as 404 HTTP Status
	ErrInvalidAPICall = errors.New("Invalid API call")

	// ErrForbidden is returned when user tries to access parts of system he isn't allowed to
	// Should be returned as 403
	ErrForbidden = errors.New("Forbidden")

	// ErrNotAuthenticated is returned when user is not authenticated (authentication token is missing or is invalid)
	// Should be returned as 401
	ErrNotAuthenticated = errors.New("Not authenticated")
)

// Error messages used when returning errors
const (
	ErrorCodeInternal               = 0   // 500; Internal server error
	ErrorCodeInvalidJSONBody        = 30  // 400; Request body contains invalid JSON
	ErrorCodeUsernameInvalid        = 201 // 401 or 403; Username is invalid
	ErrorCodePermissionInsufficient = 310 // 403; User doesn't have required permission or permission level is insufficient
	ErrorCodeValidation             = 500 // 400; Value provided for a field is not allowed (Validation error)
)

// ErrorResponse is sent back to client as JSON
type ErrorResponse struct {
	ErrorCode int
	Cause     string
}

type serverError struct {
	code      int
	cause     string
	errorType error
}

func (e serverError) Error() string {
	return e.cause
}

func (e serverError) Type() error {
	return e.errorType
}

func mapErrorTypeToHTTPStatus(err error) int {
	switch err {
	case ErrBadRequest:
		return http.StatusBadRequest
	case ErrInternal:
		return http.StatusInternalServerError
	case ErrInvalidAPICall:
		return http.StatusNotFound
	case ErrForbidden:
		return http.StatusForbidden
	case ErrNotAuthenticated:
		return http.StatusUnauthorized
	default:
		return http.StatusInternalServerError
	}
}

// return: IsServerError, ErrorCode, Cause, ErrorType
func isError(errorType error) (bool, int, string, error) {
	err, isError := errorType.(serverError)
	if !isError {
		return false, 0, "", errorType
	}

	return true, err.code, err.cause, err.Type()
}

func newError(ctx context.Context, cause string, code int, errorType, err error) error {
	logger.LogError(cause, err)
	return serverError{code, cause, errorType}
}
