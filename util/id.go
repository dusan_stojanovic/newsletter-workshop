package util

import (
	"encoding/base64"
	"math/rand"
	"sync"
	"time"
)

var (
	// GeneratePseudoRandomID creates 18 bytes long pseudo random identifier base64 encoded to 24 char long string.
	GeneratePseudoRandomID = generatePseudoRandomID
)

var (
	mutex      sync.Mutex
	pseudoRand *rand.Rand
)

func init() {
	pseudoRand = rand.New(rand.NewSource(time.Now().UnixNano()))
}

func generatePseudoRandomID() string {
	raw := make([]byte, 18, 18)
	enc := make([]byte, 24, 24)

	mutex.Lock()
	pseudoRand.Read(raw)
	mutex.Unlock()
	base64.RawURLEncoding.Encode(enc, raw)

	return string(enc)
}
