package util

var (
	// ValidateUID validates supplied UID
	ValidateUID = validateUID
)

func validateUID(uuid string) bool {
	return len(uuid) == 36 && uuid[8] == '-' && uuid[13] == '-' && uuid[18] == '-' && uuid[23] == '-' &&
		validateLowerHexString(uuid[:8]) && validateLowerHexString(uuid[9:13]) && validateLowerHexString(uuid[14:18]) &&
		validateLowerHexString(uuid[19:23]) && validateLowerHexString(uuid[24:])
}

func validateLowerHexString(hex string) bool {
	n := len(hex)
	for i := 0; i < n; i++ {
		c := hex[i]
		if (c < '0' || c > '9') && (c < 'a' || c > 'f') {
			return false
		}
	}
	return true
}
