package core

import (
	"Newsletter-Workshop/data"
	"Newsletter-Workshop/util"
	"Newsletter-Workshop/values"
	"context"
	"encoding/json"
	"io"
	"strings"
)

var (
	//CreateUser creates new user
	CreateUser = createUser

	//GetUser returns user
	GetUser = getUser

	//GetAllUsers retirns list of users
	GetAllUsers = getAllUsers

	//EditUser updates user
	EditUser = editUser

	//DeleteUser deletes user
	DeleteUser = deleteUser

	// AuthUser returns user auth token (userUID)
	AuthUser = authUser
)

func createUser(ctx context.Context, isAdmin bool, requestBody io.Reader) (response *data.UserEntity, err error) {
	type createUserRequest struct {
		Username  string
		FirstName string
		LastName  string
		UserType  int
		IsAdmin   bool
	}

	request := &createUserRequest{}
	err = json.NewDecoder(requestBody).Decode(request)
	if err != nil {
		err = util.NewError(ctx, "Failed to decode JSON", util.ErrorCodeInvalidJSONBody, util.ErrBadRequest, err)
		return
	}

	request.Username = strings.TrimSpace(request.Username)
	if request.Username == "" {
		err = util.NewError(ctx, "Trying to create user with empty username", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	request.FirstName = strings.TrimSpace(request.FirstName)
	if request.FirstName == "" {
		err = util.NewError(ctx, "Trying to create user with empty first name", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	request.LastName = strings.TrimSpace(request.LastName)
	if request.LastName == "" {
		err = util.NewError(ctx, "Trying to create user with empty last name", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	if request.UserType != values.UserTypeSubscriber && request.UserType != values.UserTypeAuthor {
		err = util.NewError(ctx, "Invalid value for user type parameter", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	if request.IsAdmin == true && isAdmin == false {
		err = util.NewError(ctx, "Only admin user can create admin user", util.ErrorCodePermissionInsufficient, util.ErrForbidden, err)
		return
	}

	usernameExists, err := data.UsernameExists(ctx, request.Username)
	if err != nil {
		err = util.NewError(ctx, "Failed to check username", util.ErrorCodeInternal, util.ErrInternal, err)
		return
	}

	if usernameExists == true {
		err = util.NewError(ctx, "Username already exists", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	response, err = data.CreateUser(ctx, request.Username, request.FirstName, request.LastName, request.UserType, request.IsAdmin)
	if err != nil {
		err = util.NewError(ctx, "Failed to create user", util.ErrorCodeInternal, util.ErrInternal, err)
		return
	}

	return
}

func getUser(ctx context.Context, isAdmin bool, requestBody io.Reader) (response *data.UserEntity, err error) {
	type getUserRequest struct {
		UserUID string
	}

	request := &getUserRequest{}
	err = json.NewDecoder(requestBody).Decode(request)
	if err != nil {
		err = util.NewError(ctx, "Failed to decode JSON", util.ErrorCodeInvalidJSONBody, util.ErrBadRequest, err)
		return
	}

	if !util.ValidateUID(request.UserUID) {
		err = util.NewError(ctx, "Invalid value for userUID parameter", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	userUID := ctx.Value(values.ContextKeyUserUID).(string)

	if isAdmin == false && userUID != request.UserUID {
		err = util.NewError(ctx, "Only admin user can get other user", util.ErrorCodePermissionInsufficient, util.ErrForbidden, err)
		return
	}

	response, err = data.GetUser(ctx, request.UserUID)
	if err != nil {
		err = util.NewError(ctx, "Failed to get user", util.ErrorCodeInternal, util.ErrInternal, err)
		return
	}

	return
}

func getAllUsers(ctx context.Context, requestBody io.Reader) (response *ArrayResponse, err error) {
	type getAllUsersRequest struct {
		RowOffset  int    `json:",omitempty"`
		RowLimit   int    `json:",omitempty"`
		SearchTerm string `json:",omitempty"`
	}

	request := &getAllUsersRequest{}
	err = json.NewDecoder(requestBody).Decode(request)
	if err != nil {
		err = util.NewError(ctx, "Failed to decode JSON", util.ErrorCodeInvalidJSONBody, util.ErrBadRequest, err)
		return
	}

	if request.RowOffset < 0 {
		err = util.NewError(ctx, "Invalid value for row offser parameter", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	if request.RowLimit < 0 || request.RowLimit > values.MaxRowLimit {
		err = util.NewError(ctx, "Invalid value for row limit parameter", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	if request.RowLimit == 0 {
		request.RowLimit = values.MaxRowLimit
	}

	users, err := data.GetAllUsers(ctx, request.SearchTerm, request.RowOffset, request.RowLimit)
	if err != nil {
		err = util.NewError(ctx, "Failed to get users", util.ErrorCodeInternal, util.ErrInternal, err)
		return
	}

	response = &ArrayResponse{
		Meta: request,
		Data: users,
	}

	return
}

func editUser(ctx context.Context, isAdmin bool, requestBody io.Reader) (err error) {
	type editUserRequest struct {
		UserUID   string
		Username  string
		FirstName string
		LastName  string
		UserType  int
		IsAdmin   bool
	}

	request := &editUserRequest{}
	err = json.NewDecoder(requestBody).Decode(request)
	if err != nil {
		err = util.NewError(ctx, "Failed to decode JSON", util.ErrorCodeInvalidJSONBody, util.ErrBadRequest, err)
		return
	}

	if !util.ValidateUID(request.UserUID) {
		err = util.NewError(ctx, "Invalid value for userUID parameter", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	request.Username = strings.TrimSpace(request.Username)
	if request.Username == "" {
		err = util.NewError(ctx, "Trying to edit user with empty username", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	request.FirstName = strings.TrimSpace(request.FirstName)
	if request.FirstName == "" {
		err = util.NewError(ctx, "Trying to edit user with empty first name", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	request.LastName = strings.TrimSpace(request.LastName)
	if request.LastName == "" {
		err = util.NewError(ctx, "Trying to edit user with empty last name", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	if request.UserType != values.UserTypeSubscriber && request.UserType != values.UserTypeAuthor {
		err = util.NewError(ctx, "Invalid value for user type parameter", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	if request.IsAdmin == true && isAdmin == false {
		err = util.NewError(ctx, "Only admin user can set admin permissions", util.ErrorCodePermissionInsufficient, util.ErrForbidden, err)
		return
	}

	userUID := ctx.Value(values.ContextKeyUserUID).(string)

	if isAdmin == false && userUID != request.UserUID {
		err = util.NewError(ctx, "Only admin user can edit other user", util.ErrorCodePermissionInsufficient, util.ErrForbidden, err)
		return
	}

	oldUserData, err := data.GetUser(ctx, request.UserUID)
	if err != nil {
		err = util.NewError(ctx, "Failed to get user", util.ErrorCodeInternal, util.ErrInternal, err)
		return
	}

	if oldUserData.Username == request.Username && oldUserData.FirstName == request.FirstName &&
		oldUserData.LastName == request.LastName && oldUserData.UserType == request.UserType && oldUserData.IsAdmin == request.IsAdmin {
		return
	}

	if oldUserData.Username != request.Username {
		var usernameExists bool
		usernameExists, err = data.UsernameExists(ctx, request.Username)
		if err != nil {
			err = util.NewError(ctx, "Failed to check username", util.ErrorCodeInternal, util.ErrInternal, err)
			return
		}

		if usernameExists == true {
			err = util.NewError(ctx, "Username already exists", util.ErrorCodeValidation, util.ErrBadRequest, err)
			return
		}
	}

	err = data.EditUser(ctx, request.UserUID, request.Username, request.FirstName, request.LastName, request.UserType, request.IsAdmin)
	if err != nil {
		err = util.NewError(ctx, "Failed to edit user", util.ErrorCodeInternal, util.ErrInternal, err)
		return
	}

	return
}

func deleteUser(ctx context.Context, requestBody io.Reader) (err error) {
	type deleteUserRequest struct {
		UserUID string
	}

	request := &deleteUserRequest{}
	err = json.NewDecoder(requestBody).Decode(request)
	if err != nil {
		err = util.NewError(ctx, "Failed to decode JSON", util.ErrorCodeInvalidJSONBody, util.ErrBadRequest, err)
		return
	}

	if !util.ValidateUID(request.UserUID) {
		err = util.NewError(ctx, "Invalid value for userUID parameter", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	err = data.DeleteUser(ctx, request.UserUID)
	if err != nil {
		err = util.NewError(ctx, "Failed to delete user", util.ErrorCodeInternal, util.ErrInternal, err)
		return
	}

	return
}

func authUser(ctx context.Context, requestBody io.Reader) (response *AuthData, err error) {
	type authUserRequest struct {
		Username string
	}

	request := &authUserRequest{}
	err = json.NewDecoder(requestBody).Decode(request)
	if err != nil {
		err = util.NewError(ctx, "Failed to decode JSON", util.ErrorCodeInvalidJSONBody, util.ErrBadRequest, err)
		return
	}

	request.Username = strings.TrimSpace(request.Username)
	if request.Username == "" {
		err = util.NewError(ctx, "Username is empty", util.ErrorCodeValidation, util.ErrBadRequest, err)
		return
	}

	authToken, err := data.AuthUser(ctx, request.Username)
	if err != nil {
		err = util.NewError(ctx, "Failed to authorize user", util.ErrorCodeInternal, util.ErrInternal, err)
		return
	}

	if authToken == "" {
		err = util.NewError(ctx, "Invalid username", util.ErrorCodeUsernameInvalid, util.ErrNotAuthenticated, err)
		return
	}

	response = &AuthData{
		AuthToken: authToken,
	}

	return
}
