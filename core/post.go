package core

import (
	"context"
	"io"
)

var (
	//CreatePost creates new posr
	CreatePost = createPost

	//GetPost returns post
	GetPost = getPost

	//GetAllPosts retirns list of posts
	GetAllPosts = getAllPosts

	//EditPost updates post
	EditPost = editPost

	//DeletePost deletes post
	DeletePost = deletePost
)

func createPost(ctx context.Context, requestBody io.Reader) (err error) {
	return
}

func getPost(ctx context.Context, requestBody io.Reader) (err error) {
	return
}

func getAllPosts(ctx context.Context, requestBody io.Reader) (err error) {
	return
}

func editPost(ctx context.Context, requestBody io.Reader) (err error) {
	return
}

func deletePost(ctx context.Context, requestBody io.Reader) (err error) {
	return
}
