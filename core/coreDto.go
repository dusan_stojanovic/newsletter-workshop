package core

// ArrayResponse is struct used to response for get all api
type ArrayResponse struct {
	Data interface{} `json:"data"`
	Meta interface{} `json:"meta"`
}

// AuthData is struct used to describe user authorization data
type AuthData struct {
	AuthToken string
}
