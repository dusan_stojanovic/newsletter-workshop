package handler

import (
	"Newsletter-Workshop/core"
	"Newsletter-Workshop/server/dbserver"
	"Newsletter-Workshop/uservalidation"
	"Newsletter-Workshop/util"
	"Newsletter-Workshop/values"
	"context"
	"io"
	"net/url"
	"strings"
)

// Request contain information about request
type Request struct {
	Authorization string
	Body          io.Reader
	URL           *url.URL
	RemoteAddr    string
}

var (
	// Handle handle all http requests
	Handle = handle
)

const (
	apiCreate = "/create"
	apiGet    = "/get"
	apiGetAll = "/getAll"
	apiEdit   = "/edit"
	apiDelete = "/delete"
)

func handle(ctx context.Context, request *Request) (response interface{}, err error) {
	if !strings.HasPrefix(request.URL.Path, "/api") {
		err = util.ErrInvalidAPICall
		return
	}

	uri := request.URL.Path[4:]

	ctx = dbserver.PrepareDbRunner(ctx)

	var userInfo *uservalidation.UserInfo

	// api/user/auth and api/user/create can be called without authorization
	if request.Authorization == "" && (strings.HasPrefix(uri, "/user/auth") || strings.HasPrefix(uri, "/user/create")) {
		userInfo = &uservalidation.UserInfo{
			IsAdmin: false,
		}
	} else {
		userInfo, err = uservalidation.ValidateUser(ctx, request.Authorization)
		if err != nil {
			err = util.ErrNotAuthenticated
			return
		}
	}

	ctx = context.WithValue(ctx, values.ContextKeyUserUID, request.Authorization)

	switch {
	case strings.HasPrefix(uri, "/user"):
		response, err = handleUser(ctx, uri[5:], userInfo, request)
	case strings.HasPrefix(uri, "/post"):
		// only authorized user can call post api
		if userInfo == nil {
			return
		}
		response, err = handlePost(ctx, uri[5:], userInfo, request)
	default:
		err = util.ErrInvalidAPICall
	}

	return
}

func handleUser(ctx context.Context, uri string, userInfo *uservalidation.UserInfo, request *Request) (interface{}, error) {
	switch uri {
	case apiCreate:
		// admin and unauthorized user can call create user api
		isAdmin := false
		if userInfo != nil {
			isAdmin = userInfo.IsAdmin
		}

		return core.CreateUser(ctx, isAdmin, request.Body)
	case apiGet:
		// only authorized user can call get api
		if userInfo == nil {
			return nil, util.ErrForbidden
		}

		return core.GetUser(ctx, userInfo.IsAdmin, request.Body)
	case apiGetAll:
		// only authorized admin user can call get all api
		if userInfo == nil || userInfo.IsAdmin == false {
			return nil, util.ErrForbidden
		}

		return core.GetAllUsers(ctx, request.Body)
	case apiEdit:
		// only authorized user can call edit api
		if userInfo == nil {
			return nil, util.ErrForbidden
		}

		return nil, core.EditUser(ctx, userInfo.IsAdmin, request.Body)
	case apiDelete:
		// only authorized admin user can call get all api
		if userInfo == nil || userInfo.IsAdmin == false {
			return nil, util.ErrForbidden
		}

		return nil, core.DeleteUser(ctx, request.Body)
	case "/auth":
		// unauthorized user can call auth user api
		return core.AuthUser(ctx, request.Body)
	default:
		return nil, util.ErrInvalidAPICall
	}
}

func handlePost(ctx context.Context, uri string, userInfo *uservalidation.UserInfo, request *Request) (response interface{}, err error) {
	switch uri {
	case apiCreate:
		// only author user can call create post
		if userInfo.UserType != values.UserTypeAuthor {
			err = util.ErrForbidden
			return
		}

		return nil, core.CreatePost(ctx, request.Body)
	case apiGet:
		return nil, core.GetPost(ctx, request.Body)
	case apiGetAll:
		return nil, core.GetAllPosts(ctx, request.Body)
	case apiEdit:
		// only author user can call create post
		if userInfo.UserType != values.UserTypeAuthor {
			err = util.ErrForbidden
			return
		}

		return nil, core.EditPost(ctx, request.Body)
	case apiDelete:
		// only author user can call create post
		if userInfo.UserType != values.UserTypeAuthor {
			err = util.ErrForbidden
			return
		}

		return nil, core.DeletePost(ctx, request.Body)
	default:
		err = util.ErrInvalidAPICall
	}

	return
}
