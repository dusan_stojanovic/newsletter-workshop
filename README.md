# Newsletter Server

## Install Go

Newsletter Server requires Go version 1.13 or later.

Instructions how to install Go are available here: https://golang.org/doc/install

Verify installation:

> go version

It should print something like this: "go version go1.13 windows/amd64" if everything is ok.

## Install dependencies

PostgresSQL database driver for Go:

> go get -u github.com/lib/pq

TOML configuration file reader library (viper) and it's dependencies:

> go get -u github.com/spf13/viper github.com/fsnotify/fsnotify

## Compile

Assuming that GOPATH environment variable has it's default value (~/go), clone the repository:

> git clone https://gitlab.com/dusan_stojanovic/newsletter-workshop.git

> cd ~/go/src/dusan_stojanovic/newsletter-workshop

Execute:

go build .\newsletter.go

This will create executable file ("newsletter.exe" on Windows).

## Database init

Create main database:

* Create database newsletter_db
* Execute "data/dbscripts/public_schema.sql"

## Run server and server deployment

* Copy "newsletter" executable file on the server.
* Copy "newsletter.toml" configuration un the same target directory.

For localhost deployment and testing just start "newsletter" executable manually.
